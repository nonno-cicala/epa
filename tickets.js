const tickets = [
    { age: 6, name: 'Luca' },
    { age: 18, name: 'Marco' }
]

/*
const getPrice = function () {
    return 2 * this.age
}

const ticketsHydrated = tickets.map(ticket => ({ ...ticket, price: getPrice }))

for (let i = 0; i < ticketsHydrated.length; i++) {
    const price = ticketsHydrated[i].price()
    console.log(price)
}
*/

//const doubleAgeTickets = tickets.map(x => x.age * 2)
//const doubleAgeTickets = tickets.map(x => Object.assign(x, { age: x.age * 2 })) /* non pura */
//const doubleAgeTickets = tickets.map(x => ({ ...x, age: x.age * 2, cognome: 'Turro' }))

//console.log(tickets, doubleAgeTickets)

//console.log(JSON.stringify(tickets))
console.log(JSON.parse('[{"age":6,"name":"Luca"},{"age":18,"name":"Marco"}]'))
//console.log(JSON.parse("[{'age':6,'name':'Luca'},{'age':18,'name':'Marco'}]")) /* non funge */
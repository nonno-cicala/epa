const square = x => x * x
const media = num => num.reduce((acc, cur) => acc + cur) / num.length
const varianza = numbers => media(numbers.map(square)) - square(media(numbers))

console.log(varianza([1, 4, 6, 7, 8, 10]))
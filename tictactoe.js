const board = [
    [' ', ' ', ' '],
    [' ', ' ', 'X'],
    [' ', ' ', ' ']
]

const boardServer = [
    ' ', 'X', ' ',
    ' ', 'O', ' ',
    ' ', 'X', ' '
]

/*
const emptyCells = board => [].concat(...board).reduce((acc, cell) => {
    if (cell === ' ') {
        return acc + 1
    }
    else {
        return acc
    }
}, 0)
*/

const isFull = board => ![].concat(...board).some(cell => cell === ' ')

const checkRow = (board, row) =>
    board[row][0] !== ' ' &&
    board[row][0] === board[row][1] && board[row][1] === board[row][2]

const checkCol = (board, col) =>
    board[0][col] !== ' ' &&
    board[0][col] === board[1][col] && board[1][col] === board[2][col]

const checkDiagDown = (board) =>
    board[0][0] !== ' ' &&
    board[0][0] === board[1][1] && board[1][1] === board[2][2]

const checkDiagUp = (board) =>
    board[0][2] != ' ' &&
    board[0][2] === board[1][1] && board[1][1] === board[2][0]

const isValid = (board, row, col) =>
    board[row][col] === ' '

const move = (board, row, col, symbol) => {
    newBoard = Array.from(board)
    if (isValid(board, row, col))
        newBoard[row][col] = symbol
    return newBoard
}

const isTerminal = board =>
    isFull(board) ||
    [0, 1, 2].some(n => checkRow(board, n) || checkCol(board, n)) ||
    checkDiagDown(board) ||
    checkDiagUp(board)

const isWinner = symbol => board =>
    [0, 1, 2].some(n =>
        (checkRow(board, n) && board[n][0] === symbol) ||
        (checkCol(board, n) && board[0][n] === symbol)
    ) ||
    (checkDiagDown(board) || checkDiagUp(board) && board[1][1] === symbol)

console.log(move(board, 1, 2, 'O'))
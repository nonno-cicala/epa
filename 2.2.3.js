const rl = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
})

const numbers = Array.from({ length: 100 }, () => Math.trunc(1 + Math.random() * 100))
//const max = Math.max(...numbers)
const findNum = (numbers, number) => {
    let indices = []
    for (let i = 0; i < numbers.length; i++) {
        if (numbers[i] === number) indices.push(i + 1)
    }
    return indices.length === 0 ? 'falso' : indices
}

rl.question('Che numero vuoi cercare? ', (num) => {
    console.log(numbers, num, findNum(numbers, +num))
    rl.close()
})
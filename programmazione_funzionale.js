const calculator = function (op, x, y) {
    return op(Number(x), Number(y))
}

const getOperation = function (opSymbol) {
    if (opSymbol === '+') {
        return function (a, b) { return a + b }
    } else if (opSymbol === '-') {
        return function (a, b) { return a - b }
    } else if (opSymbol === '*') {
        return function (a, b) { return a * b }
    } else if (opSymbol === '/') {
        return function (a, b) { return a / b }
    }
}

const rl = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.question("Inserisci l'operazione: ", (opSymbol) => {
    if (opSymbol === '+' || opSymbol === '*' || opSymbol === '-' || opSymbol === '/') {
        rl.question('Inserisci il primo operando: ', (numA) => {
            rl.question('Inserisci il secondo operando: ', (numB) => {
                rl.close()
                console.log("Il risultato dell'operazione è:", calculator(getOperation(opSymbol), numA, numB))
            })
        })
    } else {
        rl.close()
        console.log('Operazione "' + opSymbol + '" non riconosciuta')
    }
})


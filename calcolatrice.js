const rl = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
})

const op = function (opSymbol, a, b) {
    if (opSymbol === '+') {
        return a + b
    } else if (opSymbol === '*') {
        return a * b
    } else if (opSymbol === '-') {
        return a - b
    } else {
        return a / b
    }
}

rl.question("Inserisci l'operazione: ", (opSymbol) => {
    if (opSymbol === '+' || opSymbol === '*' || opSymbol === '-' || opSymbol === '/') {
        rl.question('Inserisci il primo operando: ', (numA) => {
            rl.question('Inserisci il secondo operando: ', (numB) => {
                rl.close()
                console.log('Il risultato è', op(opSymbol, +numA, +numB))
            })
        })
    } else {
        rl.close()
        console.log('Operazione non riconosciuta')
    }
})
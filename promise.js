const promise = new Promise((ftStar1, ftStar2) => {
    setTimeout(() => ftStar1('Ciao Promise'), 2000)
})

const promise2 = new Promise((ftStar1, ftStar2) => {
    setTimeout(() => ftStar1('Ciao Promise 2'), 1000)
})

const promise3 = new Promise((ftStar1, ftStar2) => {
    setTimeout(() => ftStar1('Ciao Promise 3'), 1000)
})


promise.then(hello => {
    console.log(hello)
    promise2.then(hello => {
        console.log(hello)
        promise3.then(hello => {
            console.log(hello)
        })
    })
})
console.log('Ciao Mondo')
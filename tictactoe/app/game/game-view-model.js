/*
const {
    Observable
} = require('tns-core-modules/data/observable')

const createGameViewModel = () => {
    const vm = new Observable()
    vm.set('board', [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '])
    return vm
}
*/

const {
    fromObject
} = require('tns-core-modules/data/observable')

const createGameViewModel = (board, next) => fromObject({
    board: board ? board : [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    turn: next ? next : 'X'
})

module.exports = {
    createGameViewModel
}

const {
    getFrameById
} = require("tns-core-modules/ui/frame")

const onAdd = function (args) {
    const button = args.object

    const frame = getFrameById("game-frame")
    frame.navigate("game/game")

    button.closeModal()
}

module.exports = { onAdd }
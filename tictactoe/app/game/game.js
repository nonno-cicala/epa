//let turn = 0

const {
    createGameViewModel
} = require('./game-view-model')
const {
    StackLayout
} = require('tns-core-modules/ui/layouts/stack-layout')
const {
    move,
    isTerminal,
    whoWin
} = require('../lib/state')

const result = function (page) {
    if (whoWin('X')(page.bindingContext.board)) {
        alert('X vince!')
            .then(() =>
                page.frame.navigate("game/game")
            )
    } else if (whoWin('O')(page.bindingContext.board)) {
        alert('O vince!')
            .then(() =>
                page.frame.navigate("game/game")
            )
    } else {
        alert('Pareggio')
            .then(() =>
                page.frame.navigate("game/game")
            )
    }
}

const onMove = function (args) {
    const cell = args.object
    const board = move(
        cell.index,
        cell.bindingContext.board,
        cell.bindingContext.turn
    )
    /*
    if (!cell.class.match("[XO]")) {
        cell.className = `${cell.index % 2 === 0 ? "even" : "odd"} ${turn % 2 === 0 ? "O" : "X"}`
        turn++
    } else {
        cell.className = `${cell.index % 2 === 0 ? "even" : "odd"}`
        turn--
    }
    */
    const nextTurn = cell.bindingContext.turn === 'X' ? 'O' : 'X'
    const page = cell.parent.parent
    page.bindingContext = createGameViewModel(board, nextTurn)
    createBoard(page)
    if (isTerminal(page.bindingContext.board)) {
        result(page)
    } else {
        // AI Move
        console.log('AI Move')
        const httpModule = require('tns-core-modules/http')
        httpModule.request({
            //url: 'https://cpw.sonofrio.com/ai/api/ai/move',
            url: 'https://home.nonnocicala.it/ai/api/ai/move',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            content: JSON.stringify({
                level: 'random',
                board: page.bindingContext.board
            })
        })
            .then(response => response.content.toJSON())
            .then(board => {
                console.log(board)
                page.bindingContext = createGameViewModel(board, nextTurn === 'X' ? 'O' : 'X')
            })
            .then(() => createBoard(page))
            .then(() => {
                if (isTerminal(page.bindingContext.board)) {
                    result(page)
                }
            })
            .catch(e => console.log(e))
    }
}

const createBoard = page => {
    const vm = page.bindingContext
    const boardContainer = page.getViewById('game-board')
    boardContainer.removeChildren()
    for (let i = 0; i < vm.board.length; i++) {
        const classes = [
            i % 2 === 0 ? 'even' : 'odd',
            vm.board[i] !== ' ' ? vm.board[i] : ''
        ]
        const stack = new StackLayout()
        stack.col = i % 3
        stack.col = i % 3
        stack.row = Math.floor(i / 3)
        stack.index = i
        stack.class = classes.join(' ')
        stack.addEventListener('tap', onMove)
        boardContainer.addChild(stack)
    }
}

const onNavigatingTo = function (args) {
    const page = args.object
    page.bindingContext = createGameViewModel()
    createBoard(page)
}

const onAddGame = function (args) {
    const button = args.object
    button.showModal(
        "game/addgame-root", {},
        () => {
            console.log("Modal closed");
        },
        false
    )
}

const onLayoutChanged = function (args) {
    const board = require("tns-core-modules/ui/core/view").getViewById(args.object, "game-board")
    const totalSpace = board.getActualSize()
    let sideSpace
    if (totalSpace.width < totalSpace.height) {
        sideSpace = (totalSpace.height - totalSpace.width) / 2
        board.paddingTop = sideSpace
        board.paddingBottom = sideSpace
        board.paddingLeft = 0
        board.paddingRight = 0
    } else {
        sideSpace = (totalSpace.width - totalSpace.height) / 2
        board.paddingTop = 0
        board.paddingBottom = 0
        board.paddingLeft = sideSpace
        board.paddingRight = sideSpace
    }
}

module.exports = {
    onAddGame,
    onNavigatingTo,
    onLayoutChanged
}

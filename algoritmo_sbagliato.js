const x = 2

if (x % 2 === 0) {
    console.log(x, "Divisibile per 2")
} else if (x % 3 === 0) {
    console.log(x, 'Divisibile per 3')
} else {
    console.log(x, 'Non è divisibile per 2 o per 3')
}